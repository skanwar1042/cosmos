import React, { useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import Toast from "react-native-toast-message";
import { createTable } from "./Service/db";

import Login from "./Screens/Login";
import Users from "./Screens/Users";
import UserDetails from "./Screens/UserDetails";
import Header from "./Screens/components/Header";
import AuthenticationProvider from "./Screens/AuthenticationProvider";

const Stack = createStackNavigator();

const App = () => {
  useEffect(() => {
    const initDB = async () => {
      try {
        await createTable();
      } catch (error) {
        console.log(error);
      }
    };
    initDB();
  }, []);

  return (
    <AuthenticationProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen
            name="Login"
            component={Login}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Users"
            component={Users}
            options={{
              headerTitle: (props) => <Header {...props} title="Employees" />,
            }}
          />
          <Stack.Screen
            name="UserDetails"
            component={UserDetails}
            options={{
              headerTitle: "Employee details",
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
      <Toast id="appToast" />
    </AuthenticationProvider>
  );
};

export default App;
