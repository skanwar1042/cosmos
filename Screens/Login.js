import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import Toast from "react-native-toast-message";
import { getItemFromStore, setItemInStore } from "../Utils/secureStorage";
import { useAuth } from "./AuthenticationProvider";

const LoginPage = () => {
  const navigation = useNavigation();
  const { login } = useAuth();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    getItemFromStore("@Cosmos:user")
      .then((employee) => {
        if (employee) {
          navigation.navigate("Users");
        }
      })
      .catch((err) => console.log(err));
  }, [navigation]);

  const clearTextbox = () => {
    setUsername("");
    setPassword("");
  };

  const handleLogin = () => {
    const isValid = login(username, password);
    if (isValid) {
      clearTextbox();
      navigation.navigate("Users");
    } else {
      Toast.show({
        type: "error",
        text1: "Invalid username or password",
      });
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Login to Cosmos</Text>
      <TextInput
        id="txtUsername"
        style={styles.input}
        placeholder="Username"
        value={username}
        onChangeText={(text) => setUsername(text.toLowerCase())}
      />
      <TextInput
        id="txtPassword"
        style={styles.input}
        placeholder="Password"
        secureTextEntry
        value={password}
        onChangeText={(text) => setPassword(text.toLowerCase())}
      />
      <TouchableOpacity
        id="btnLogin"
        style={styles.loginButton}
        onPress={handleLogin}
      >
        <Text style={styles.buttonText}>Login</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  input: {
    width: "100%",
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 16,
    paddingHorizontal: 10,
  },
  loginButton: {
    backgroundColor: "#3498db",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 8,
  },
  buttonText: {
    color: "#fff",
    fontSize: 16,
    fontWeight: "bold",
  },
});

export default LoginPage;
