import React, { useCallback, useContext, useState } from "react";
import { clearStorage, setItemInStore } from "../Utils/secureStorage";

const AuthenticationContext = React.createContext();

function AuthenticationProvider({ children }) {
  const [isLoggedIn, setIsLoggedIn] = useState(true);

  const login = useCallback((username, password) => {
    if (username === "admin" && password === "admin") {
      setItemInStore("@Cosmos:user", {
        user: "admin",
      });
      setIsLoggedIn(true);
      return true;
    }
    setIsLoggedIn(false);
    return false;
  }, []);

  const logout = useCallback(() => {
    setIsLoggedIn(false);
    clearStorage();
  }, []);

  return (
    <AuthenticationContext.Provider value={{ isLoggedIn, login, logout }}>
      {children}
    </AuthenticationContext.Provider>
  );
}

export const useAuth = () => useContext(AuthenticationContext);

export default AuthenticationProvider;
