import EncryptedStorage from 'react-native-encrypted-storage';

export const setItemInStore = async (key, data) => {
  try {
    return await EncryptedStorage.setItem(key, JSON.stringify(data));
  } catch (error) {
    return null;
  }
};

export const getItemFromStore = async key => {
  try {
    const value = await EncryptedStorage.getItem(key);
    if (value !== undefined) {
      const data = JSON.parse(value);
      return data;
    }
    return null;
  } catch (error) {
    return null;
  }
};

export const deletedItemFromStore = async key => {
  try {
    return await EncryptedStorage.removeItem(key);
  } catch (error) {
    return null;
  }
};

export const clearStorage = async () => {
  try {
    return await EncryptedStorage.clear();
  } catch (error) {
    return null;
  }
};
